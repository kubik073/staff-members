{$obj_prefix = "`$block.block_id`000"}

{if $block.properties.outside_navigation == "Y"}
    <div class="owl-theme ty-owl-controls">
        <div class="owl-controls clickable owl-controls-outside" id="owl_outside_nav_{$block.block_id}">
            <div class="owl-buttons">
                <div id="owl_prev_{$obj_prefix}" class="owl-prev"><i class="ty-icon-left-open-thin"></i></div>
                <div id="owl_next_{$obj_prefix}" class="owl-next"><i class="ty-icon-right-open-thin"></i></div>
            </div>
        </div>
    </div>
{/if}

<div id="scroll_list_{$block.block_id}" class="owl-carousel">
    {foreach from=$items item="member" name="for_members"}
            {include file="common/image.tpl" assign="object_img" class="ty-grayscale" image_width=$block.properties.thumbnail_width image_height=$block.properties.thumbnail_width images=$member.main_pair no_ids=true lazy_load=true obj_id="scr_`$block.block_id`000`$member.member_id`"}
            <div class="ty-center">
                {$object_img nofilter}
                <span class="ty-block"><strong>{$member.name}</strong></span>
                <span class="ty-block">{$member.post nofilter}</span>
                <span class="ty-block" id="email_{$member.member_id}_{$obj_prefix}">
                    <a class="cm-ajax cm-post" href="{"staff.get_email&member_id=`$member.member_id`&prefix=`$obj_prefix`"|fn_url}" data-ca-target-id="email_{$member.member_id}_{$obj_prefix}">{__("show_email")}</a>
                <!--email_{$member.member_id}_{$obj_prefix}--></span>
            </div>
    {/foreach}
</div>
{include file="common/scroller_init.tpl" items=$items prev_selector="#owl_prev_`$obj_prefix`" next_selector="#owl_next_`$obj_prefix`"}
