<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']  == 'POST') {

    fn_trusted_vars('member_data');
    $suffix = '';

    if ($mode == 'm_delete') {
        foreach ($_REQUEST['member_ids'] as $v) {
            fn_delete_staff_member($v);
        }

        $suffix = '.manage';
    }

    if ($mode == 'update') {
        
        $allow_save = fn_check_allow_save_member_data($_REQUEST['member_data']);
        
        if ($allow_save === false) {
            // Some error occured
            fn_save_post_data('member_data');
            fn_set_notification('E', __('error'), __('staff_member_update_error'));
            return array(CONTROLLER_STATUS_REDIRECT, !empty($_REQUEST['member_id']) ? 'staff.update?member_id=' . $_REQUEST['member_id'] : 'staff.add');
        }
    
        $member_id = fn_update_staff_member($_REQUEST['member_data'], $_REQUEST['member_id'], DESCR_SL);

        $suffix = ".update?member_id=$member_id";
    }

    if ($mode == 'delete') {
        if (!empty($_REQUEST['member_id'])) {
            fn_delete_staff_member($_REQUEST['member_id']);
        }

        $suffix = '.manage';
    }

    return array(CONTROLLER_STATUS_OK, 'staff' . $suffix);
}

if ($mode == 'update') {
    $member = fn_get_staff_member_data($_REQUEST['member_id'], DESCR_SL);

    if (empty($member)) {
        return array(CONTROLLER_STATUS_NO_PAGE);
    }
    $member_name = fn_get_staff_member_name($_REQUEST['member_id']);

    Tygh::$app['view']->assign('member', $member);
    Tygh::$app['view']->assign('member_name', $member_name);

} elseif ($mode == 'manage') {

    list($members, ) = fn_get_staff_members_list(array(), DESCR_SL);

    Tygh::$app['view']->assign('members', $members);
} elseif ($mode == 'add') {
    $member_data = fn_restore_post_data('member_data');
    Tygh::$app['view']->assign('member', $member_data);
}

