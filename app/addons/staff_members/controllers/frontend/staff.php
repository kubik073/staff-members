<?php

use Tygh\Registry;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

if ($_SERVER['REQUEST_METHOD']  == 'POST') {
    if ($mode == 'get_email') {
        if (!empty($_REQUEST['prefix']) && !empty($_REQUEST['member_id'])) {
            
            $member_data = db_get_row('SELECT email, user_id FROM ?:staff_members WHERE member_id = ?i', $_REQUEST['member_id']);
            if (!empty($member_data['email'])) {
                $email = $member_data['email'];
            } elseif (!empty($member_data['user_id'])) {
                $email = db_get_field('SELECT email FROM ?:users WHERE user_id = ?i', $member_data['user_id']);
            } else {
                $email = __('no_email');
            }
            
            Tygh::$app['view']->assign('email', $email);
            Tygh::$app['view']->assign('prefix', $_REQUEST['prefix']);
            Tygh::$app['view']->assign('member_id', $_REQUEST['member_id']);
            Tygh::$app['view']->display('addons/staff_members/components/email.tpl');
            
        }
        exit();
    }
}