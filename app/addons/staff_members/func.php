<?php

use Tygh\Registry;
use Tygh\Languages\Languages;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_get_staff_members_list($params = array(), $lang_code = CART_LANGUAGE) {
    $default_params = array(
        'items_per_page' => 0,
    );

    $params = array_merge($default_params, $params);

    $sortings = array(
        'position' => '?:staff_members.position',
    );

    $condition = $limit = '';

    if (!empty($params['limit'])) {
        $limit = db_quote(' LIMIT 0, ?i', $params['limit']);
    }

    $sorting = db_sort($params, $sortings, 'position', 'asc');

    $fields = array (
        '?:staff_members.*',
        '?:staff_member_descriptions.post',
        '?:users.firstname as u_firstname',
        '?:users.lastname as u_lastname',
        '?:users.email as u_email'
    );

    $members = db_get_hash_array(
        "SELECT ?p FROM ?:staff_members " .
        "LEFT JOIN ?:staff_member_descriptions ON ?:staff_member_descriptions.member_id = ?:staff_members.member_id AND ?:staff_member_descriptions.lang_code = ?s" .
        "LEFT JOIN ?:users ON ?:users.user_id = ?:staff_members.user_id " .
        "WHERE 1 ?p ?p ?p",
        'member_id', implode(", ", $fields), $lang_code, $condition, $sorting, $limit
    );
    
    if (!empty($members)) {
        if (isset($params['get_images']) && !empty($params['get_images'])) {
            $member_ids = array_keys($members);
            $images = fn_get_image_pairs($member_ids, 'staff_member', 'M', true, false, $lang_code);
        } else {
            $images = array();
        }
        
        foreach ($members as $member_id => $member) {
            $name = (!empty($member['firstname'])) ? $member['firstname'] : $member['u_firstname'];
            $name .= ' ';
            $name .= (!empty($member['lastname'])) ? $member['lastname'] : $member['u_lastname'];
            $members[$member_id]['name'] = $name;
            if (empty($member['email']) && !empty($member['u_email'])) {
                $members[$member_id]['email'] = $member['u_email'];
            }
            if (!empty($images)) {
                $members[$member_id]['main_pair'] = !empty($images[$member_id]) ? reset($images[$member_id]) : array();
            }
        }
    }
    return array($members, $params);
}

function fn_get_staff_member_data($member_id, $lang_code = CART_LANGUAGE) {
    $fields = $joins = array();
    $condition = '';

    $fields = array (
        '?:staff_members.*',
        '?:staff_member_descriptions.post'
    );

    $joins[] = db_quote("LEFT JOIN ?:staff_member_descriptions ON ?:staff_member_descriptions.member_id = ?:staff_members.member_id AND ?:staff_member_descriptions.lang_code = ?s", $lang_code);

    $condition = db_quote("WHERE ?:staff_members.member_id = ?i", $member_id);

    $member = db_get_row("SELECT " . implode(", ", $fields) . " FROM ?:staff_members " . implode(" ", $joins) ." $condition");

    if (!empty($member)) {
        $member['main_pair'] = fn_get_image_pairs($member_id, 'staff_member', 'M', true, false, $lang_code);
    }
    return $member;
}

function fn_delete_staff_member($member_id) {
    if (!empty($member_id)) {
        db_query("DELETE FROM ?:staff_members WHERE member_id = ?i", $member_id);
        db_query("DELETE FROM ?:staff_member_descriptions WHERE member_id = ?i", $member_id);

        fn_delete_image_pairs($member_id, 'staff_member');
    }
}

function fn_update_staff_member($data, $member_id, $lang_code = DESCR_SL) {
    if (!empty($member_id)) {
        db_query("UPDATE ?:staff_members SET ?u WHERE member_id = ?i", $data, $member_id);
        db_query("UPDATE ?:staff_member_descriptions SET ?u WHERE member_id = ?i AND lang_code = ?s", $data, $member_id, $lang_code);
    } else {
        $member_id = $data['member_id'] = db_query("REPLACE INTO ?:staff_members ?e", $data);

        foreach (Languages::getAll() as $data['lang_code'] => $v) {
            db_query("REPLACE INTO ?:staff_member_descriptions ?e", $data);
        }
    }
    $pair_data = fn_attach_image_pairs('staff_member', 'staff_member', $member_id, $lang_code);
    return $member_id;
}

function fn_get_staff_member_name($member_id) {
    $member_data = db_get_row('SELECT a.firstname, a.lastname, b.firstname as u_firstname, b.lastname as u_lastname FROM ?:staff_members as a LEFT JOIN ?:users as b ON a.user_id = b.user_id WHERE a.member_id = ?i', $member_id);
    if (empty($member_data)) {
        return '';
    }
    $name = (!empty($member_data['firstname'])) ? $member_data['firstname'] : $member_data['u_firstname'];
    $name .= ' ';
    $name .= (!empty($member_data['lastname'])) ? $member_data['lastname'] : $member_data['u_lastname'];
    
    return $name;
}

function fn_check_allow_save_member_data(&$member_data) {
    $allow_save = false;
    if (!empty($member_data['firstname']) && !empty($member_data['lastname']) && !empty($member_data['email'])) {
        $allow_save = true;
    } elseif (!empty($member_data['user_id'])) {
        $check_user = db_get_field('SELECT user_id FROM ?:users WHERE user_id = ?i', $member_data['user_id']);
        if (!empty($check_user)) {
            $allow_save = true;
        } else {
            fn_set_notification('W', __('warning'), __('user_not_exists'));
            $member_data['user_id'] = 0;
        }
    }
    return $allow_save;
}



