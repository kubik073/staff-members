<?php

$schema['staff_members'] = array (
    'content' => array (
        'items' => array(
            'type' => 'enum',
            'object' => 'members',
            'items_function' => 'fn_get_staff_members_list',
            'remove_indent' => true,
            'hide_label' => true,
            'fillings' => array(
                'all' => array(
                    'params' => array(
                        'get_images' => true
                    )
                )
            )
        )
    ),
    'templates' => array (
        'addons/staff_members/blocks/staff_members.tpl' => array(),
    ),
    'wrappers' => 'blocks/wrappers',
    'cache' => array(
        'update_handlers' => array(
            'staff_members', 'staff_member_descriptions'
        )
    )
);

return $schema;
