{if $member}
    {assign var="id" value=$member.member_id}
{else}
    {assign var="id" value=0}
{/if}

{capture name="mainbox"}

<form action="{""|fn_url}" method="post" class="form-horizontal form-edit  " name="members_form" enctype="multipart/form-data">
<input type="hidden" class="cm-no-hide-input" name="fake" value="1" />
<input type="hidden" class="cm-no-hide-input" name="member_id" value="{$id}" />

{capture name="tabsbox"}
<div id="content_general">
    
        <div class="control-group">
        <label for="elm_member_firstname" class="control-label">{__("firstname")}</label>
        <div class="controls">
        <input type="text" name="member_data[firstname]" id="elm_member_firstname" value="{$member.firstname}" size="25" class="input-long" /></div>
    </div>

    <div class="control-group">
        <label for="elm_member_lastname" class="control-label">{__("lastname")}</label>
        <div class="controls">
        <input type="text" name="member_data[lastname]" id="elm_member_lastname" value="{$member.lastname}" size="25" class="input-long" /></div>
    </div>

    <div class="control-group">
        <label for="elm_member_email" class="control-label cm-email">{__("email")}</label>
        <div class="controls">
        <input type="text" name="member_data[email]" id="elm_member_email" value="{$member.email}" size="25" class="input-long" /></div>
    </div>
    
    <div class="control-group">
        <label for="elm_member_pos" class="control-label">{__("position")}</label>
        <div class="controls">
        <input type="text" name="member_data[position]" id="elm_member_pos" value="{$member.position}" size="25" class="input-micro" /></div>
    </div>

    <div class="control-group">
        <label class="control-label cm-required" for="elm_member_description">{__("post")}:</label>
        <div class="controls">
            <textarea id="elm_member_description" name="member_data[post]" cols="35" rows="8" class="cm-wysiwyg input-large">{$member.post}</textarea>
        </div>
    </div>
    
    <div class="control-group">
        <label class="control-label">{__("image")}</label>
        <div class="controls">
            {include file="common/attach_images.tpl" image_name="staff_member" image_object_type="staff_member" image_pair=$member.main_pair image_object_id=$id no_detailed=true hide_titles=true}
        </div>
    </div>

    <div class="control-group">
        <label for="elm_member_user" class="control-label">{__("user_id")}</label>
        <div class="controls">
        <input type="text" name="member_data[user_id]" id="elm_member_user" value="{$member.user_id}" size="25" class="input-micro" /></div>
    </div>
    
</div>
{/capture}
{include file="common/tabsbox.tpl" content=$smarty.capture.tabsbox active_tab=$smarty.request.selected_section track=true}

{capture name="buttons"}
    {include file="buttons/save_cancel.tpl" but_role="submit-link" but_target_form="members_form" but_name="dispatch[staff.update]" save=$id}
{/capture}
    
</form>

{/capture}

{if !$id}
    {assign var="title" value=__("members.new_member")}
{else}
    {assign var="title" value="{__("members.editing_member")}: `$member_name`"}
{/if}
{include file="common/mainbox.tpl" title=$title content=$smarty.capture.mainbox buttons=$smarty.capture.buttons select_languages=true}

