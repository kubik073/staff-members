{capture name="mainbox"}

<form action="{""|fn_url}" method="post" name="members_form" class=" " enctype="multipart/form-data">
<input type="hidden" name="fake" value="1" />

{if $members}
<table class="table table-middle">
<thead>
<tr>
    <th width="1%" class="left">
        {include file="common/check_items.tpl" class="cm-no-hide-input"}</th>
    <th>{__("member")}</th>
    <th>{__("post")}</th>
    <th>{__("email")}</th>
    <th>{__("user")}</th>
    <th width="6%">&nbsp;</th>
</tr>
</thead>
{foreach from=$members item=member}
<tr>
    <td class="left">
        <input type="checkbox" name="member_ids[]" value="{$member.member_id}" class="cm-item " /></td>
    <td class="">
        <a class="row-status" href="{"staff.update?member_id=`$member.member_id`"|fn_url}">{$member.name}</a>
    </td>
    <td>
        {$member.post nofilter}
    </td>
    <td class="nowrap row-status ">
        <a href="mailto:{$member.email|escape:url}">{$member.email}</a>
    </td>
    <td>
        {if $member.user_id}
            <a href="{"profiles.update&user_id=`$member.user_id`"|fn_url}">#{$member.user_id}</a>
        {else}
            {__("none")}
        {/if}
    </td>
    <td>
        {capture name="tools_list"}
            <li>{btn type="list" text=__("edit") href="staff.update?member_id=`$member.member_id`"}</li>
            <li>{btn type="list" class="cm-confirm cm-post" text=__("delete") href="staff.delete?member_id=`$member.member_id`"}</li>
        {/capture}
        <div class="hidden-tools">
            {dropdown content=$smarty.capture.tools_list}
        </div>
    </td>
</tr>
{/foreach}
</table>
{else}
    <p class="no-items">{__("no_data")}</p>
{/if}

{capture name="buttons"}
    {capture name="tools_list"}
        {if $members}
            <li>{btn type="delete_selected" dispatch="dispatch[staff.m_delete]" form="members_form"}</li>
        {/if}
    {/capture}
    {dropdown content=$smarty.capture.tools_list}
{/capture}
{capture name="adv_buttons"}
    {include file="common/tools.tpl" tool_href="staff.add" prefix="top" hide_tools="true" title=__("add_member") icon="icon-plus"}
{/capture}

</form>

{/capture}
{include file="common/mainbox.tpl" title=__("staff_members") content=$smarty.capture.mainbox buttons=$smarty.capture.buttons adv_buttons=$smarty.capture.adv_buttons select_languages=true}
